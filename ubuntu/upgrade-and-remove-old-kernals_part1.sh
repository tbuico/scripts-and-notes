#!/bin/bash

# Upgrade
printf "%s\n""\e[0;32m" && echo "Upgrading OS" && printf "\e[0m\n"
apt -y update
apt -y dist-upgrade

# Cleanup 1
printf "%s\n""\e[0;32m" && echo "Cleaning Up After Upgrades" && printf "\e[0m\n"
apt -y autoremove
apt -y autoclean
rm -rf /var/cache/apt/archives/*.deb

# Reboot 1
printf "%s\n""\e[0;32m" && echo "System Will Now Reboot" && printf "\e[0m\n"
reboot
