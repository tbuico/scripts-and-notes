#!/bin/bash

# Fix for weird issues in Ubuntu, Also frees up a lot of space
rm -rf /var/cache/apt/archives/*.deb
cd /var/lib/apt
mv lists lists.old
mkdir -p lists/partial
cd

# Delete all old old kernel versions
#dpkg --get-selections | grep linux-image #(Shows all kernel versions)
sudo apt-get -y remove --purge linux-image-4.4.0-31-generic
sudo apt-get -y remove --purge linux-image-4.4.0-66-generic
#sudo apt-get -y remove --purge linux-image-4.4.0-72-generic
apt -y update
apt -y full-upgrade
