#!/bin/bash

apt -y update
apt -y full-upgrade
apt -y install fail2ban
echo "[DEFAULT]" > /etc/fail2ban/jail.local
echo "# Ban hosts for one hour:" >> /etc/fail2ban/jail.local
echo "bantime = 3600" >> /etc/fail2ban/jail.local
echo "# Override /etc/fail2ban/jail.d/00-firewalld.conf:" >> /etc/fail2ban/jail.local
echo "banaction = iptables-multiport" >> /etc/fail2ban/jail.local
echo "[sshd]" >> /etc/fail2ban/jail.local
echo "enabled = true" >> /etc/fail2ban/jail.local
echo "[DEFAULT]" > /etc/fail2ban/jail.d/sshd.local
echo "# Ban hosts for one hour:" >> /etc/fail2ban/jail.d/sshd.local
echo "bantime = -1" >> /etc/fail2ban/jail.d/sshd.local
echo "findtime = 600" >> /etc/fail2ban/jail.d/sshd.local
echo "maxretry = 3" >> /etc/fail2ban/jail.d/sshd.local
echo "# Override /etc/fail2ban/jail.d/00-firewalld.conf:" >> /etc/fail2ban/jail.d/sshd.local
echo "banaction = iptables-multiport" >> /etc/fail2ban/jail.d/sshd.local
echo "[sshd]" >> /etc/fail2ban/jail.d/sshd.local
echo "enabled = true" >> /etc/fail2ban/jail.d/sshd.local
echo "filter   = sshd" >> /etc/fail2ban/jail.d/sshd.local
echo "action   = iptables[name=SSH, port=ssh, protocol=tcp]" >> /etc/fail2ban/jail.d/sshd.local
echo "#           sendmail-whois[name=SSH, dest=fail2ban@clamshelldata.com, sender=fail2ban@clamshelldata.com]" >> /etc/fail2ban/jail.d/sshd.local
echo "logpath  = /var/log/secure" >> /etc/fail2ban/jail.d/sshd.local
ufw allow ssh
echo "y" | sudo ufw enable

echo "[INFO]" > /etc/fail2ban/info.txt
echo "Here are some commands that you may find useful when using fail2ban." >> /etc/fail2ban/info.txt
echo "For more info on fail2ban vist this page: https://linux.die.net/man/1/fail2ban-client" >> /etc/fail2ban/info.txt
echo " " >> /etc/fail2ban/info.txt
echo "1) Check the Fal2Ban Status:" >> /etc/fail2ban/info.txt
echo "fail2ban-client status" >> /etc/fail2ban/info.txt
echo " " >> /etc/fail2ban/info.txt
echo "2) Detailed information about sshd jail:" >> /etc/fail2ban/info.txt
echo "fail2ban-client status sshd" >> /etc/fail2ban/info.txt
echo " " >> /etc/fail2ban/info.txt
echo "3) Tracking Failed login entries:" >> /etc/fail2ban/info.txt
echo "cat /var/log/secure | grep 'Failed password'" >> /etc/fail2ban/info.txt
echo " " >> /etc/fail2ban/info.txt
echo "4) Checking the banned IPs by Fail2Ban:" >> /etc/fail2ban/info.txt
echo "iptables -L -n" >> /etc/fail2ban/info.txt
echo " " >> /etc/fail2ban/info.txt
echo "5) Unbanning an IP address:" >> /etc/fail2ban/info.txt
echo "fail2ban-client set sshd unbanip xxx.xxx.xxx.xxx" >> /etc/fail2ban/info.txt
echo " " >> /etc/fail2ban/info.txt
