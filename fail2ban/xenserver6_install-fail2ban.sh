yum --enablerepo=extras install epel-release
yum --enablerepo=epel --enablerepo=base install mc htop screen fail2ban fail2ban-systemd
yum update -y selinux-policy*
echo "[DEFAULT]" > /etc/fail2ban/jail.local
echo "# Ban hosts for one hour:" >> /etc/fail2ban/jail.local
echo "bantime = 3600" >> /etc/fail2ban/jail.local
echo "# Override /etc/fail2ban/jail.d/00-firewalld.conf:" >> /etc/fail2ban/jail.local
echo "banaction = iptables-multiport" >> /etc/fail2ban/jail.local
echo "[sshd]" >> /etc/fail2ban/jail.local
echo "enabled = true" >> /etc/fail2ban/jail.local
echo "[DEFAULT]" > /etc/fail2ban/jail.d/sshd.local
echo "# Ban hosts for one hour:" >> /etc/fail2ban/jail.d/sshd.local
echo "bantime = -1" >> /etc/fail2ban/jail.d/sshd.local
echo "findtime = 600" >> /etc/fail2ban/jail.d/sshd.local
echo "maxretry = 3" >> /etc/fail2ban/jail.d/sshd.local
echo "# Override /etc/fail2ban/jail.d/00-firewalld.conf:" >> /etc/fail2ban/jail.d/sshd.local
echo "banaction = iptables-multiport" >> /etc/fail2ban/jail.d/sshd.local
echo "[sshd]" >> /etc/fail2ban/jail.d/sshd.local
echo "enabled = true" >> /etc/fail2ban/jail.d/sshd.local
echo "filter   = sshd" >> /etc/fail2ban/jail.d/sshd.local
echo "action   = iptables[name=SSH, port=ssh, protocol=tcp]" >> /etc/fail2ban/jail.d/sshd.local
echo "#           sendmail-whois[name=SSH, dest=fail2ban@clamshelldata.com, sender=fail2ban@clamshelldata.com]" >> /etc/fail2ban/jail.d/sshd.local
echo "logpath  = /var/log/secure" >> /etc/fail2ban/jail.d/sshd.local
systemctl enable firewalld
systemctl start firewalld
systemctl enable fail2ban
systemctl start fail2ban

### Check the Fal2Ban Status ###
## fail2ban-client status ##

### Detailed information about sshd jail ###
## fail2ban-client status sshd ##

### Tracking Failed login entries ###
## cat /var/log/secure | grep 'Failed password' ##

### Checking the banned IPs by Fail2Ban ###
## iptables -L -n ##

### Unbanning an IP address ###
## fail2ban-client set sshd unbanip IPADDRESS ##
