#!/bin/bash

# https://wiki.ubuntu.com/X/Config/Input#Example%3a%20Disabling%20middle-mouse%20button%20paste%20on%20a%20scrollwheel%20mouse

i=$(xinput list | grep 'ETPS/2 Elantech Touchpad')
x=$(echo -n "$i" | cut -f 2 -d '=' | cut -f 1)
xinput set-button-map "$x" 1 1 3 4 5 6 7
