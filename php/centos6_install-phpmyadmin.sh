#!/bin/bash

yum -y upgrade
yum -y install epel-release httpd yum-utils mysql mysql-server
rpm -Uvh http://rpms.remirepo.net/enterprise/remi-release-6.rpm
yum-config-manager --enable remi-php54
yum -y install php php-common php-mysql
/sbin/chkconfig --levels 235 mysqld on
service mysqld start
mysql_secure_installation
yum -y install phpmyadmin
nano /etc/httpd/conf.d/phpMyAdmin.conf
## Change all 127.0.0.1	IPs to VPN or workstation IP ##
## look into adding "Require all granted" so PhpMyAdmin may be accessed from other IPs ##
service httpd restart
yum -y upgrade
yum clean all
### Go to: http://VPS_IP_address/phpmyadmin ###
