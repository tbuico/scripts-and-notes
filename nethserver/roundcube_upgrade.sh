#!/bin/bash

yum -y upgrade
rc_download=$(wget -qO- https://github.com/roundcube/roundcubemail/releases/latest | grep -v "complete.tar.gz.asc"| grep -E "complete.tar.gz" | grep -Eo 'href="[^\"]+"' | sed "s|href=|https://github.com/|" | tr -d '""')
wget $rc_download
tar xf roundcubemail-*.tar.gz
rm -rf roundcubemail-*.tar.gz
cd roundcubemail-*
mkdir -p /usr/share/roundcubemail/config
./bin/installto.sh /usr/share/roundcubemail
cd /usr/share/roundcubemail
chown -R apache:apache config
chmod -R g+w config
