#!/bin/bash

#  Determine the size of the new swap file in megabytes and multiply by `1024` to determine the number of blocks. For example, the block size of a `5 GB` swap file is `5120000`.

dd if=/dev/zero of=/swapfile bs=1024 count=2048000
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile

# Set swappiness from `30` to `10`
echo "/swapfile swap swap defaults 0 0" >> /etc/fstab
echo "10" > /proc/sys/vm/swappiness

# Set cache pressure from `100` to `50`
echo "vm.swappiness = 10" >> /etc/sysctl.conf
sudo sysctl vm.vfs_cache_pressure=50
echo "vm.vfs_cache_pressure = 50" >> /etc/sysctl.conf

yum -y update
yum -y localinstall http://mirror.nethserver.org/nethserver/nethserver-release-7.rpm
nethserver-install
# nethserver should now be running, if it is not or you saw this error:
# "Failed to start nethserver-system-init.service: Operation refused, unit nethserver-system-init.service may be requested by dependency only.
# See system logs and 'systemctl status nethserver-system-init.service' for details."
# Then run the following:
# `cat /lib/systemd/system/nethserver-system-init.service`
# Check what the "ExecStart=" is, it should be this: /sbin/e-smith/signal-event system-init
# Run the "ExecStart=" and nethserver should now be running.
