#!/bin/bash

### BEGIN SCRIPT JOBS ###
{
echo "   -----   script started   -----   "
ping 8.8.8.8
echo "   -----   script ended   -----   "
} 2>&1 | tee /root/Logs/testscript.log
