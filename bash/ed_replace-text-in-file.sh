### goal: find "$globals['update'] = 0;" then replace with "$globals['update'] = 1;" in /usr/local/virtualizor/universal.php ###
#This will find the first line with the word "update" and then on that line, replace 0 with 1.
ed -s /usr/local/virtualizor/universal.php <<< $'/update/s/0/1/g\nw'
