1. Determine the size of the new swap file in megabytes and multiply by `1024` to determine the number of blocks. For example, the block size of a `5 GB` swap file is `5120000`.

2. At a shell prompt as root, type the following command with count being equal to the desired block size:
    ```
    dd if=/dev/zero of=/swapfile bs=1024 count=5120000
    ```

3. We should adjust the permissions on our swap file so that it isn't readable by anyone besides the root account. Allowing other users to read or write to this file would be a huge security risk. We can lock down the permissions with `chmod`:
    ```
    chmod 600 /swapfile
    ```

4. This will restrict both read and write permissions to the root account only. We can verify that the swap file has the correct permissions by using `ls -lh`:
    ```
    ls -lh /swapfile
    ```

5. Setup the swap file with the command:
    ```
    mkswap /swapfile
    ```

6. To enable the swap file immediately but not automatically at boot time:
    ```
    swapon /swapfile
    ```

7. To enable it at boot time, edit `/etc/fstab` to include the following entry by using this command:
    ```
    echo "/swapfile swap swap defaults 0 0" >> /etc/fstab
    ```

8. After adding the new swap file and enabling it, verify it is enabled by viewing the output of the command
    ```
    swapon -s
    free -m
    ```

9. Set The swappiness from the default of `30` to something better for a cloud server like `10`:
    ```
    echo "10" > /proc/sys/vm/swappiness
    echo "vm.swappiness = 10" >> /etc/sysctl.conf
    ```

10. As it is currently configured, our system removes inode information from the cache far too quickly. We can set this to a more conservative setting, like `50`, by using `sysctl`:
    ```
    sudo sysctl vm.vfs_cache_pressure=50
    echo "vm.vfs_cache_pressure = 50" >> /etc/sysctl.conf
    ```
