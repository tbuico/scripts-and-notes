#!/bin/bash

# Variables:
# ----------

## Names:
  script=git-run
## Paths:
  git_home=/home/tom/Git
  cd_clamshelldata_nc_installer="$git_home"/clamshelldata-nc-installer
  cd_clamshelldata_wp_installer="$git_home"/clamshelldata-wp-installer
  cd_gif_reactions="$git_home"/gif-reactions
  cd_grey_goo_syntax="$git_home"/grey-goo-syntax
  cd_scripts_and_notes="$git_home"/scripts-and-notes
  cd_rclone=/home/tom/rclone
  log=/home/tom/logs/"$script".log
## Error Messages:
  fatal_1='fatal: The remote end hung up unexpectedly'
  fatal_2='fatal: Could not read from remote repository.'
  fatal_3='fatal: unable to connect a socket (Connection timed out)'
## Colors:
  ### Reset Color:
    Color_Off='\e[0m\n'       # Text Reset
  ### Regular Colors:
    Black='\e[0;30m'        # Black
    Red='\e[0;31m'          # Red
    Green='\e[0;32m'        # Green
    Yellow='\e[0;33m'       # Yellow
    Blue='\e[0;34m'         # Blue
    Purple='\e[0;35m'       # Purple
    Cyan='\e[0;36m'         # Cyan
    White='\e[0;37m'        # White
  ### Underline:
    UBlack='\e[4;30m'       # Black
    URed='\e[4;31m'         # Red
    UGreen='\e[4;32m'       # Green
    UYellow='\e[4;33m'      # Yellow
    UBlue='\e[4;34m'        # Blue
    UPurple='\e[4;35m'      # Purple
    UCyan='\e[4;36m'        # Cyan
    UWhite='\e[4;37m'       # White

# Functions:
# ----------

clamshelldata_nc_installer() {
  start_clamshelldata_nc_installer=`date +%s`
  printf "\n   ${Green}Preforming "$script" on ${UGreen}clamshelldata_nc_installer${Color_Off}\n"
  cd "$cd_clamshelldata_nc_installer"
  if [[ $(git remote show origin) =~ "out of date" ]]
  then
    printf "\n      ${Red}☠ | Remote is out of date with ${URed}clamshelldata_nc_installer${Color_Off}\n"
    git pull -v
  elif [[ $(git status) =~ "added to commit" ]]
  then
    printf "\n      ${Red}☠ | ${URed}clamshelldata_nc_installer${Red} is out of date with Remote${Color_Off}\n"
    git add -v .
    git commit -m "$(date '+%F %T')"
    git push -v --progress
    if [[ $(grep "$fatal_1" "$log") ]]
    then
      printf "\n   ${Yellow}"$script" encountered the following error with ${UYellow}clamshelldata_nc_installer${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_1}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date +%F\ %T` & exited unexpectedly during clamshelldata_nc_installer after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    elif [[ $(grep "$fatal_2" "$log") ]]
    then
      printf "\n   ${Yellow}"$script" encountered the following error with ${UYellow}clamshelldata_nc_installer${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_2}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date +%F\ %T` & exited unexpectedly during clamshelldata_nc_installer after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    elif [[ $(grep "$fatal_3" "$log") ]]
    then
      printf "\n   ${Yellow}"$script" encountered the following error with ${UYellow}VIA_Portable_Apps${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_3}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date +%F\ %T` & exited unexpectedly during VIA_Portable_Apps after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    else
      end=`date +%s`
      runtime_clamshelldata_nc_installer=$((end-start_clamshelldata_nc_installer))
      printf "\n${UPurple}   -----   clamshelldata_nc_installer finished in $runtime_clamshelldata_nc_installer seconds   -----   ${Color_Off}\n"
    fi
  else
    printf "\n      ${Blue}✓ | ${UBlue}clamshelldata_nc_installer${Blue} is up to date${Color_Off}\n"
  fi
}

clamshelldata_wp_installer() {
  start_clamshelldata_wp_installer=`date +%s`
  printf "\n   ${Green}Preforming "$script" on ${UGreen}clamshelldata_wp_installer${Color_Off}\n"
  cd "$cd_clamshelldata_wp_installer"
  if [[ $(git remote show origin) =~ "out of date" ]]
  then
    printf "\n      ${Red}☠ | Remote is out of date with ${URed}clamshelldata_wp_installer${Color_Off}\n"
    git pull -v
  elif [[ $(git status) =~ "added to commit" ]]
  then
    printf "\n      ${Red}☠ | ${URed}clamshelldata_wp_installer${Red} is out of date with Remote${Color_Off}\n"
    git add -v .
    git commit -m "$(date '+%F %T')"
    git push -v --progress
    if [[ $(grep "$fatal_1" "$log") ]]
    then
      printf "\n   ${Yellow}"$script" encountered the following error with ${UYellow}clamshelldata_wp_installer${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_1}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date +%F\ %T` & exited unexpectedly during clamshelldata_wp_installer after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    elif [[ $(grep "$fatal_2" "$log") ]]
    then
      printf "\n   ${Yellow}"$script" encountered the following error with ${UYellow}clamshelldata_wp_installer${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_2}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date +%F\ %T` & exited unexpectedly during clamshelldata_wp_installer after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    elif [[ $(grep "$fatal_3" "$log") ]]
    then
      printf "\n   ${Yellow}"$script" encountered the following error with ${UYellow}VIA_Portable_Apps${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_3}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date +%F\ %T` & exited unexpectedly during VIA_Portable_Apps after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    else
      end=`date +%s`
      runtime_clamshelldata_wp_installer=$((end-start_clamshelldata_wp_installer))
      printf "\n${UPurple}   -----   clamshelldata_wp_installer finished in $runtime_clamshelldata_wp_installer seconds   -----   ${Color_Off}\n"
    fi
  else
    printf "\n      ${Blue}✓ | ${UBlue}clamshelldata_wp_installer${Blue} is up to date${Color_Off}\n"
  fi
}

gif_reactions() {
  start_gif_reactions=`date +%s`
  printf "\n   ${Green}Preforming "$script" on ${UGreen}gif_reactions${Color_Off}\n"
  cd "$cd_gif_reactions"
  if [[ $(git remote show origin) =~ "out of date" ]]
  then
    printf "\n      ${Red}☠ | Remote is out of date with ${URed}gif_reactions${Color_Off}\n"
    git pull -v
  elif [[ $(git status) =~ "added to commit" ]]
  then
    printf "\n      ${Red}☠ | ${URed}gif_reactions${Red} is out of date with Remote${Color_Off}\n"
    git add -v .
    git commit -m "$(date '+%F %T')"
    git push -v --progress
    if [[ $(grep "$fatal_1" "$log") ]]
    then
      printf "\n   ${Yellow}"$script" encountered the following error with ${UYellow}gif_reactions${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_1}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date +%F\ %T` & exited unexpectedly during gif_reactions after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    elif [[ $(grep "$fatal_2" "$log") ]]
    then
      printf "\n   ${Yellow}"$script" encountered the following error with ${UYellow}gif_reactions${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_2}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date +%F\ %T` & exited unexpectedly during gif_reactions after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    elif [[ $(grep "$fatal_3" "$log") ]]
    then
      printf "\n   ${Yellow}"$script" encountered the following error with ${UYellow}VIA_Portable_Apps${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_3}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date +%F\ %T` & exited unexpectedly during VIA_Portable_Apps after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    else
      end=`date +%s`
      runtime_gif_reactions=$((end-start_gif_reactions))
      printf "\n${UPurple}   -----   gif_reactions finished in $runtime_gif_reactions seconds   -----   ${Color_Off}\n"
    fi
  else
    printf "\n      ${Blue}✓ | ${UBlue}gif_reactions${Blue} is up to date${Color_Off}\n"
  fi
}

grey_goo_syntax() {
  start_grey_goo_syntax=`date +%s`
  printf "\n   ${Green}Preforming "$script" on ${UGreen}grey_goo_syntax${Color_Off}\n"
  cd "$cd_grey_goo_syntax"
  if [[ $(git remote show origin) =~ "out of date" ]]
  then
    printf "\n      ${Red}☠ | Remote is out of date with ${URed}grey_goo_syntax${Color_Off}\n"
    git pull -v
  elif [[ $(git status) =~ "added to commit" ]]
  then
    printf "\n      ${Red}☠ | ${URed}grey_goo_syntax${Red} is out of date with Remote${Color_Off}\n"
    git add -v .
    git commit -m "$(date '+%F %T')"
    git push -v --progress
    if [[ $(grep "$fatal_1" "$log") ]]
    then
      printf "\n   ${Yellow}"$script" encountered the following error with ${UYellow}grey_goo_syntax${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_1}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date +%F\ %T` & exited unexpectedly during grey_goo_syntax after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    elif [[ $(grep "$fatal_2" "$log") ]]
    then
      printf "\n   ${Yellow}"$script" encountered the following error with ${UYellow}grey_goo_syntax${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_2}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date +%F\ %T` & exited unexpectedly during grey_goo_syntax after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    elif [[ $(grep "$fatal_3" "$log") ]]
    then
      printf "\n   ${Yellow}"$script" encountered the following error with ${UYellow}VIA_Portable_Apps${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_3}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date +%F\ %T` & exited unexpectedly during VIA_Portable_Apps after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    else
      end=`date +%s`
      runtime_grey_goo_syntax=$((end-start_grey_goo_syntax))
      printf "\n${UPurple}   -----   grey_goo_syntax finished in $runtime_grey_goo_syntax seconds   -----   ${Color_Off}\n"
    fi
  else
    printf "\n      ${Blue}✓ | ${UBlue}grey_goo_syntax${Blue} is up to date${Color_Off}\n"
  fi
}

scripts_and_notes() {
  start_scripts_and_notes=`date +%s`
  printf "\n   ${Green}Preforming "$script" on ${UGreen}scripts_and_notes${Color_Off}\n"
  cd "$cd_scripts_and_notes"
  if [[ $(git remote show origin) =~ "out of date" ]]
  then
    printf "\n      ${Red}☠ | Remote is out of date with ${URed}scripts_and_notes${Color_Off}\n"
    git pull -v
  elif [[ $(git status) =~ "added to commit" ]]
  then
    printf "\n      ${Red}☠ | ${URed}scripts_and_notes${Red} is out of date with Remote${Color_Off}\n"
    git add -v .
    git commit -m "$(date '+%F %T')"
    git push -v --progress
    if [[ $(grep "$fatal_1" "$log") ]]
    then
      printf "\n   ${Yellow}"$script" encountered the following error with ${UYellow}scripts_and_notes${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_1}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date +%F\ %T` & exited unexpectedly during scripts_and_notes after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    elif [[ $(grep "$fatal_2" "$log") ]]
    then
      printf "\n   ${Yellow}"$script" encountered the following error with ${UYellow}scripts_and_notes${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_2}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date +%F\ %T` & exited unexpectedly during scripts_and_notes after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    elif [[ $(grep "$fatal_3" "$log") ]]
    then
      printf "\n   ${Yellow}"$script" encountered the following error with ${UYellow}VIA_Portable_Apps${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_3}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date +%F\ %T` & exited unexpectedly during VIA_Portable_Apps after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    else
      end=`date +%s`
      runtime_scripts_and_notes=$((end-start_scripts_and_notes))
      printf "\n${UPurple}   -----   scripts_and_notes finished in $runtime_scripts_and_notes seconds   -----   ${Color_Off}\n"
    fi
  else
    printf "\n      ${Blue}✓ | ${UBlue}scripts_and_notes${Blue} is up to date${Color_Off}\n"
  fi
}

rclone() {
  start_rclone=`date +%s`
  printf "\n   ${Green}Preforming "$script" on ${UGreen}rclone${Color_Off}\n"
  cd "$cd_rclone"
  if [[ $(git remote show origin) =~ "out of date" ]]
  then
    printf "\n      ${Red}☠ | Remote is out of date with ${URed}rclone${Color_Off}\n"
    git pull -v
  elif [[ $(git status) =~ "added to commit" ]]
  then
    printf "\n      ${Red}☠ | ${URed}rclone${Red} is out of date with Remote${Color_Off}\n"
    git add -v .
    git commit -m "$(date '+%F %T')"
    git push -v --progress
    if [[ $(grep "$fatal_1" "$log") ]]
    then
      printf "\n   ${Yellow}"$script" encountered the following error with ${UYellow}rclone${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_1}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date +%F\ %T` & exited unexpectedly during rclone after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    elif [[ $(grep "$fatal_2" "$log") ]]
    then
      printf "\n   ${Yellow}"$script" encountered the following error with ${UYellow}rclone${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_2}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date +%F\ %T` & exited unexpectedly during rclone after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    elif [[ $(grep "$fatal_3" "$log") ]]
    then
      printf "\n   ${Yellow}"$script" encountered the following error with ${UYellow}VIA_Portable_Apps${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_3}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date +%F\ %T` & exited unexpectedly during VIA_Portable_Apps after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    else
      end=`date +%s`
      runtime_rclone=$((end-start_rclone))
      printf "\n${UPurple}   -----   rclone finished in $runtime_rclone seconds   -----   ${Color_Off}\n"
    fi
  else
    printf "\n      ${Blue}✓ | ${UBlue}rclone${Blue} is up to date${Color_Off}\n"
  fi
}

# Script:
# ----------

## Start logging
{
start=`date +%s`
printf "\n${UPurple}   -----   "$script" Started @ `date +%F\ %T`   -----   ${Color_Off}\n"

## Sync all the Git dirs to gitlab
clamshelldata_nc_installer
clamshelldata_wp_installer
gif_reactions
scripts_and_notes
rclone

## End logging
end=`date +%s`
runtime=$((end-start))
printf "\n${UPurple}   -----   "$script" ended @ `date +%F\ %T` & Finished in $runtime seconds   -----   ${Color_Off}\n"

} 2>&1 | tee "$log"
