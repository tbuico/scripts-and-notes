#!/bin/bash

mysql -u root -e "DROP USER 'nextclouduser'@'localhost'"
mysql -u root -e "DROP DATABASE nextcloud"
mysql -u root -e "SET PASSWORD FOR root@localhost=PASSWORD('')"
mysql -u root -e "SET PASSWORD FOR root@127.0.0.1=PASSWORD('')"
