yum -y update
yum -y install epel-release httpd
sed -i 's/^/#&/g' /etc/httpd/conf.d/welcome.conf
sed -i "s/Options Indexes FollowSymLinks/Options FollowSymLinks/" /etc/httpd/conf/httpd.conf
sed -i 's/^/#&/g' /etc/httpd/conf.modules.d/00-dav.conf
systemctl start httpd.service && systemctl enable httpd.service
yum -y clean all
rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm
yum -y update
yum -y install php70w php70w-common php70w-mysql php70w-xml php70w-process php70w-intl php70w-mcrypt php70w-cli php70w-json php70w-gd php70w-mbstring php70w-zip php70w-imap php70w-pcre php70w-zlib php70w-curl
cp /etc/php.ini /etc/php.ini.bak
sed -i "s/memory_limit = 128M/memory_limit = 256M/" /etc/php.ini
sed -i "s/post_max_size = 8M/post_max_size = 50M/" /etc/php.ini
sed -i "s/upload_max_filesize = 2M/upload_max_filesize = 50M/" /etc/php.ini
echo "<?php" > /var/www/html/phpinfo.php
echo "//Show all information, defaults to INFO_ALL" >> /var/www/html/phpinfo.php
echo "phpinfo();" >> /var/www/html/phpinfo.php
echo "?>" >> /var/www/html/phpinfo.php
systemctl restart httpd
## (You can go to xxx.xxx.xxx.xxx/phpinfo.php to check php settings) ##
yum -y install mariadb mariadb-server
systemctl start mariadb.service && systemctl enable mariadb.service
