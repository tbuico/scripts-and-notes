# First get your workstation ready for local test kitchen development using vagrant on top of virtualbox
1. Install VirtualBox, https://www.virtualbox.org/wiki/Downloads
2. Install Vagrant, https://www.vagrantup.com/downloads.html

### Get the learn_chef_httpd cookbook from GitHub
```
mkdir ~/learn-chef/cookbooks
cd ~/learn-chef/cookbooks
git clone https://github.com/learn-chef/learn_chef_httpd.git
cd ~/learn-chef/cookbooks/learn_chef_httpd
```

Examine the Test Kitchen configuration file

`cat ~/learn-chef/cookbooks/learn_chef_httpd/.kitchen.yml`

#### Create the Test Kitchen instance

From the ~/learn-chef/cookbooks/learn_chef_httpd directory, run kitchen list to see what's in the kitchen.

`kitchen list`

#### Create the instance now by running:

`kitchen create`

### Apply the learn_chef_httpd cookbook to your Test Kitchen instance
#### Now run 'kitchen converge' to apply the cookbook to the CentOS virtual machine.

`kitchen converge`

###### Test Kitchen exits with exit code `0`. Run the following to check the exit code.

`echo $?`

###### If you receive a result other than `0`, fix the errors that were reported.

### Verify that your Test Kitchen instance is configured as expected
###### The 'kitchen login' command creates an SSH connection into your instance and enables you to explore your test instance and verify its configuration
###### Or you can run kitchen exec to run a single command.

`kitchen exec -c 'curl localhost'`

### Delete the Test Kitchen instance
Run the kitchen destroy command to delete it.

`kitchen destroy`

Run kitchen list to verify that there is no created kitchen

`kitchen list`

# Test for, & Resolve a failure in test kitchen
#### Assign an owner to the home page

`nano /home/tom/Documents/Training/learn-chef/cookbooks/learn_chef_httpd/recipes/default.rb` #replace contents with the following

```
#
# Cookbook Name:: learn_chef_httpd
# Recipe:: default
#
# Copyright (c) 2016 The Authors, All Rights Reserved.
package 'httpd'

service 'httpd' do
  action [:enable, :start]
end

template '/var/www/html/index.html' do
  source 'index.html.erb'
  mode '0644'
  owner 'web_admin'
  group 'web_admin'
end
```

Apply the changes to your test instance

`kitchen converge`

###### Note the failure
###### Also, note that Test Kitchen returns with a non-zero exit code.

`exit $?`

## Resolve the failure
`nano /home/tom/Documents/Training/learn-chef/cookbooks/learn_chef_httpd/recipes/default.rb` #replace contents with the following

```
#
# Cookbook Name:: learn_chef_httpd
# Recipe:: default
#
# Copyright (c) 2016 The Authors, All Rights Reserved.
package 'httpd'

service 'httpd' do
  action [:enable, :start]
end

group 'web_admin'

user 'web_admin' do
  group 'web_admin'
  system true
  shell '/bin/bash'
end

template '/var/www/html/index.html' do
  source 'index.html.erb'
  mode '0644'
  owner 'web_admin'
  group 'web_admin'
end
```

#### Run kitchen converge to see if the latest changes fix the errors

`kitchen converge`

Check if the the exit code is `0`

`echo $?`

Run kitchen exec to verify the contents of your web server's home page.

`kitchen exec -c 'curl localhost'`

Also verify that the web_admin user is the owner of the home page and that the home page has the expected file permissions.

`kitchen exec -c 'stat /var/www/html/index.html'`

# Success
##### run kitchen destroy to delete your test instance.

`kitchen destroy`
