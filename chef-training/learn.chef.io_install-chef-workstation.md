# Install chefDK
```
curl https://omnitruck.chef.io/install.sh | sudo bash -s -- -P chefdk -c stable -v 0.18.30
```

### Set up your .chef directory on your workstation
1. In a web browser on your workstation, got to the domain or ip of the chef server.
2. Sign in, click the administrtion tab, select the organization, on the side bar click starter kit, then download starter kit
3. Move the chef-starter.zip file to the desired location and extract
4. Verify that the chef-repo/.chef/knife.rb has the correct chef_server_url (if not, change it)

```
cd chef-repo/
knife ssl fetch
knife ssl check
```
